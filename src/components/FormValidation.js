import React, { Component } from 'react';

class FormValidation extends Component {
  render() {
    return ( 
        <div>
        <div className="login-form__validation-steps clear-fix">
            <span className={"login-form__validation-steps__item " + this.props.validStepOne }></span>
            <span className={"login-form__validation-steps__item " + this.props.validStepTwo }></span>
            <span className={"login-form__validation-steps__item " + this.props.validStepThree }></span>
        </div>
        <ul className="login-form__validation-list">
              <li className={"login-form__validation-list__item " + this.props.validCharacters }>Pelo menos 6 caracteres</li>
              <li className={"login-form__validation-list__item " + this.props.validUpper }>Pelo menos 1 letra maiúscula</li>
              <li className={"login-form__validation-list__item " + this.props.validNumber }>Pelo menos 1 número</li>
        </ul>
     </div>
    );
  }
}

export default FormValidation;
