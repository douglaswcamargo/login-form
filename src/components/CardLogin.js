import React, { Component } from 'react';
import LoginForm from './LoginForm';
import logo from '../logo_polvo_colorido.svg';

class CardLogin extends Component {
  render() {
    return ( 
      <div className="card card--login">
        <div className="logo-wrapper">
          <img src={logo} className="logo-wrapper__logo" alt="logo" />
        </div>
        <div className="welcome-message">
          <div className="welcome-message--purple">{this.props.message}</div>
        </div>
        <div>
          <LoginForm />
        </div>
      </div>
     
    );
  }
}

export default CardLogin;
