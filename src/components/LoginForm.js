import React, { Component } from 'react';
import FormValidation from './FormValidation';

class LoginForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            passwordConfirm: '',
            submitDisabled: true,
            validEmail: 'default',
            validCharacters: 'default',
            validUpper: 'default',
            validNumber: 'default',
            validPassword: 'default',
            validPasswordConfirm: 'default',
            validStepOne: 'default',
            validStepTwo: 'default',
            validStepThree: 'default',
            validForm: false
        }
  
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
  
    onChange(event) {
        this.setState(
                    { [event.target.name]: event.target.value }, 
                    this.handleValidation(event.target.name,event.target.value)
                    );
    }

    handleValidation(name, value) {
        
        switch (name) {
            case 'email':
                let emailValidation = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                this.setState({validEmail: emailValidation ? 'success' : 'danger'}, this.validateForm());
                break;
            case 'passwordConfirm':
                let firstEntry = this.state.password;
                let secondEntry = value;
                this.setState({
                    validPasswordConfirm: firstEntry ===  secondEntry ? 'success' : 'danger',
                }, this.validateForm());
            break;
            case 'password':
                let totalValidsteps = 0;
                let stepOneValidation = 'default';
                let stepTwoValidation = 'default';
                let stepThreeValidation = 'default';
                let passwordValidation  = 'default';
                let lengthValidation = value.length >= 6;
                let upperValidation = /[A-Z]/.test(value);
                let numberValidation = /[0-9]/.test(value);
                totalValidsteps = (lengthValidation ? 1 : 0) + (upperValidation  ? 1 : 0) + (numberValidation ? 1 : 0);
                if( totalValidsteps === 3 ) {
                    stepOneValidation = 'success';
                    stepTwoValidation = 'success';
                    stepThreeValidation = 'success';
                    passwordValidation = 'success';
                } else if ( totalValidsteps === 2) {
                    stepOneValidation = 'warning';
                    stepTwoValidation = 'warning';
                    passwordValidation = 'warning';
                } else if ( totalValidsteps <= 1) {
                    stepOneValidation = 'danger';
                    passwordValidation = 'danger'
                }

                this.setState({
                    validCharacters: lengthValidation ? 'success' : 'danger',
                    validUpper: upperValidation ? 'success' : 'danger',
                    validNumber: numberValidation ? 'success' : 'danger',
                    validPassword : passwordValidation,
                    validStepOne: stepOneValidation,
                    validStepTwo: stepTwoValidation,
                    validStepThree: stepThreeValidation,
                }, this.validateForm());
            break;
        
            default:
                break;
        }
        
    }

    validateForm(){
        let isNotValid = true;
        setTimeout(() => {
        if(this.state.validPassword === 'success' && this.state.validEmail === 'success' && this.state.validPasswordConfirm === 'success') {
            isNotValid = false;
        }
        this.setState({submitDisabled: isNotValid, validForm: !isNotValid});
        }, 300);
    }

    onSubmit(event) {
        event.preventDefault();
        if( this.state.validForm) {
            alert("Valid Form!")
        }
        return;
    }
  render() {
    return ( 
          <form className="login-form">
          <div className="login-form__input-field">
            <label className="login-form__input-field__label" htmlFor="email">E-mail</label>
            <input className={"login-form__input-field__input " + this.state.validEmail } id="email" type="email" onChange={this.onChange} value={this.state.email} name="email" autoComplete="email"/>
          </div>
          <div className="login-form__input-field">
            <label className="login-form__input-field__label" htmlFor="password">Senha</label>
            <input className={"login-form__input-field__input " + this.state.validPassword } id="password" type="password" onChange={this.onChange} value={this.state.password} name="password" autoComplete="new-password"/>
          </div>
            <FormValidation {...this.state} />
          <div className="login-form__input-field">
            <label className="login-form__input-field__label" htmlFor="passwordConfirm">Confirme sua senha</label>
            <input className={"login-form__input-field__input " + this.state.validPasswordConfirm } disabled={ this.state.validPassword !== 'success' } id="passwordConfirm" type="password" onChange={this.onChange} value={this.state.onChange} name="passwordConfirm"  autoComplete="new-password"/>
          </div>
          <button onClick={this.onSubmit} disabled={this.state.submitDisabled} className={ "btn btn--purple " + (this.state.submitDisabled ? 'btn--disabled' : '')}>Cadastrar</button>
          </form>
    );
  }
}

export default LoginForm;
