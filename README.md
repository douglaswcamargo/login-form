## Sobre

Para realizar este teste foram utilizados:

* React, com o `create-react-app` [link](https://github.com/facebookincubator/create-react-app).
* `Sass`.

## Instruções

* Para rodar é necessário ter o `NodeJs` instalado, após isso basta clonar este repositório e rodar `npm install` e depois  `npm start` para iniciar o server Dev.
* Os arquivos prontos para produção estão disponíveis na pasta `build`.
